syntax on
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set nu
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
set nocompatible
set backspace=2

set cc=+1
function Switchwidth()
    if &textwidth == 0
        set textwidth=90
    elseif &textwidth == 90
        set textwidth=80
    elseif &textwidth == 80
        set textwidth=70
    elseif &textwidth == 70
        set textwidth=60
    elseif &textwidth == 60
        set textwidth=0
    else
        set textwidth=0
    endif
    "set cc=+1
    echo &textwidth " columns"
endfunction

" Key mappings
map <silent> <F8> :call Switchwidth()<cr>

set list
set listchars=tab:▸\ ,trail:␣,nbsp:⍽
function SwitchIndent()
    if !exists("b:indenttype")
        let b:indenttype = 0

        let b:save_expandtab   = &l:expandtab
        let b:save_smarttab    = &l:smarttab
        let b:save_tabstop     = &l:tabstop
        let b:save_shiftwidth  = &l:shiftwidth
        let b:save_softtabstop = &l:softtabstop
    endif

    if b:indenttype == 0
        echo "Indent Style: 2 (space)"
        let b:indenttype += 1

        let b:save_expandtab   = &l:expandtab
        let b:save_smarttab    = &l:smarttab
        let b:save_tabstop     = &l:tabstop
        let b:save_shiftwidth  = &l:shiftwidth
        let b:save_softtabstop = &l:softtabstop

        let &l:expandtab   = 1
        let &l:smarttab    = 0
        let &l:tabstop     = 8
        let &l:shiftwidth  = 2
        let &l:softtabstop = 2

    elseif b:indenttype == 1
        echo "Indent Style: 4 (space)"
        let b:indenttype += 1

        let &l:expandtab   = 1
        let &l:smarttab    = 0
        let &l:tabstop     = 8
        let &l:shiftwidth  = 4
        let &l:softtabstop = 4

    elseif b:indenttype == 2
        echo "Indent Style: 8 (space)"
        let b:indenttype += 1

        let &l:expandtab   = 1
        let &l:smarttab    = 0
        let &l:tabstop     = 8
        let &l:shiftwidth  = 8
        let &l:softtabstop = 8

    elseif b:indenttype == 3
        echo "Indent Style: 8 (tab)"
        let b:indenttype += 1

        let &l:expandtab   = 0
        let &l:smarttab    = 0
        let &l:tabstop     = 8
        let &l:shiftwidth  = 8
        let &l:softtabstop = 8

    else
        echo "Indent Style: ORIGINAL"
        let b:indenttype = 0

        let &l:expandtab   = b:save_expandtab
        let &l:smarttab    = b:save_smarttab
        let &l:tabstop     = b:save_tabstop
        let &l:shiftwidth  = b:save_shiftwidth
        let &l:softtabstop = b:save_softtabstop
    endif
endfunction

map <silent> <F7> :call SwitchIndent()<cr>

au BufWinLeave ?* mkview
au BufWinEnter ?* silent loadview
colorscheme default
set hlsearch
